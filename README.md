# RetrofitABM
Preliminary version of an Agent-based modelof homeowners' private investment decisions regarding energy-efficient retrofitting measures in a neighbourhood.

The model is developed using the Python Mesa Framework for agent-based modelling. It is initiated by running "model.py". 

The data regarding building archetypes present in the neighbourhood, retrofit options suitable for those building archetypes and energy prices are input in the form of an excel file. 
