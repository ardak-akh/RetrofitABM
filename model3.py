from mesa import Model
from mesa.datacollection import DataCollector
from agents3 import Homeowner
from schedule3 import StagedActivation
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import random

random.seed(1)
pd.set_option('display.max_columns', None)

class RetrofitABM(Model):
    """
    This model ... (description)
    User-settable PARAMETERS """
    
    def __init__(
            self,
            households=10, # actually it depends on the buildings in the list
            buildings=pd.read_excel('input_buildings3.xlsx', sheet_name='buildings'),
            retrofit_options=pd.ExcelFile('input_retrofit3.xlsx'),
            # for now prices from 2017 to 2021 
            prices=pd.read_excel('input_prices3.xlsx', sheet_name='prices')):
        
        #fixing the dataframes 
        prices.set_index('Energy type', inplace=True)
        
        self.schedule = StagedActivation(self, ["interact", "interest", 
                                                      "plan", "influence"],
                                         shuffle=False)
        
         # opinion dynamics parameters
        #self.threshold = 0.5 #can be between 0 and 2, but 0,2 are extreme cases
        self.mu = 0.3 #mu, high mu - more responsive to others' opinions
        
        # agents
        # self.agenttypes = [Homeowner] # Intermediary - StagedActivationByType
        # types of buildings
        num_arch = len(buildings.index) # how many archetypes of buildings we have
        num_per_arch = [] # number of buildings per archetype listed
        for i in range(num_arch):
            num_per_arch.append(buildings['Number'][i])
        #print(num_per_arch) 
        
        #self.schedule.add(Intermediary(100 , self, households))
        
        
        # create homeowner agents (they own buildings from the input excel)
        count=0
        for i in range(num_arch):
            for j in range(num_per_arch[i]):
                h = Homeowner(j+count, self, buildings.iloc[i], retrofit_options, prices)
                self.schedule.add(h)
            count+=1+j
        
        # datacollectors
        self.datacollector = DataCollector(
            model_reporters={"(Cumulative) number of adopters": num_adoption,
                             "Quantity of options adopted": num_options},
            agent_reporters={"Adoption status": "adoption_status",
                            "Adopted options": "adopted_option_name"})
        
    def step(self):
        # tell all the agents in the model to run their step function
        print('*********MODEL STEP STARTS********************')
        self.schedule.step()
        self.datacollector.collect(self)
        print('*********MODEL STEP ENDS********************')
        
    # def run_model(self):
    #     for i in range(self.run_time):
    #         self.step()


def num_adoption(model):
    """
    A method to calculate the total number of adopters and other model-level
    statistics
    """
    agent_adoptions = [agent.adoption_status for agent in model.schedule.agents]
    return sum(agent_adoptions)

def num_options(model):
    """
    A method to calculate the total number of adopters and other model-level
    statistics
    """
    adopted_options = [agent.adopted_option_name for agent in model.schedule.agents]
    unique = dict(zip(adopted_options,[adopted_options.count(i) for i in adopted_options]))
    return unique


model = RetrofitABM()

for i in range(2):
    model.step()
    
# plot the colected data
adoption_status = model.datacollector.get_agent_vars_dataframe()
print(adoption_status)

total_adopted_options = model.datacollector.get_model_vars_dataframe()
print(total_adopted_options)

# df1 = adoption_status.groupby(['Step'])['Adoption status'].sum().astype(int)
# print(df1)
# ax1=df1.plot.bar(x='Step', y='Number of agents adopted', rot=0, color='darkorange')
# ax1.yaxis.set_major_locator(MaxNLocator(integer=True))
# ax1.set(xlabel="Step", ylabel="Number of agents adopted")

# plt.savefig('adoption_step.png', dpi=300)

# df2 = adoption_status.groupby(['Adopted packages']).count()
# print(df2)
# ax2=df2.plot.bar(rot=0)
# ax2.yaxis.set_major_locator(MaxNLocator(integer=True))
# ax2.set(xlabel="Step", ylabel="Number and types of packages adopted")
# plt.savefig('adoption_package.png', dpi=300)
