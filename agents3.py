""" ABM of net-zero integrated retrofitting:
    Agents
"""

from mesa import Agent
import pandas as pd
import random
import numpy as np
from math import log


### constants
# for opinion dynamics 
        

class Homeowner(Agent):
    
    """ A Homeowner agent is an owner-occuppier of a single family 
    building (defined in Building Class). Agents are heterogeneous 
    according to: 
        - owned building (archetype)
        - ? budget
        - opinion (could influence prob_interest)
        - prob_interest
        - weight factors
        - ...
    """
    
    def __init__(self, unique_id, model, building, retrofit_options, prices): #removed prices
        # initialize the parent class with required parameters
        super().__init__(unique_id, model)
        
        #### indicators (with initial values)
        self.adoption_status = False
        self.adopted_option = None
        self.adopted_option_name = None
        self.preferred = None
        self.profitable = None
        self.informed = 0 # number of times that an agent has been informed
        self.interested = False
        #self.retrofit_needed = False 
        
        ### agent's building (assigned at model initialization, from buildings.xlsx)
        self.building = building 
        self.area_arch = self.building['Reference floor area [m2]'] 
        self.area = self.area_arch + round(random.uniform(-0.25, 0.25),2)*self.area_arch
        print('AREA', self.area)
        #### heterogeneity attributes
        #self.budget = random.randint(1000, 10000)
        
        # weight of financial and environmental factors, generated randomly
        num = round(random.uniform(0,1), 1)
        self.factors = np.array([num, 1-num])
        
        # assigning opinions
        opi_list = np.arange(-1,1,0.05).round(2)
        self.opi = random.choice(opi_list) 
        unc_list = np.arange(0.1,1.9,0.05).round(2)
        self.unc = random.choice(unc_list) 
        print('opinion and uncertainty: ', self.opi, self.unc)
        
        # probability of being interested in retrofitting 
        # normalized opi to scale (0.0,1.0) --- integrate unc?
        self.prob_interest = round((self.opi+1)/2,2)
        print('Prob_interest: ', self.prob_interest)
        # energy prices --> need to convert into future projections
        self.prices = prices # needed for planning stage

        # suitable retrofit options for an agent's building
        self.r_o = pd.read_excel(retrofit_options, sheet_name=self.building["Building archetype"])
        
        self.r_o.set_index('Name', inplace=True)
        
        ### parameters for NPV calculation
        self.discount_rate = 0.05
        self.lifetime = 30
        
        # cost reuction factor due to group purchase
        self.group_factor = 0.05 # change to a function for cost reduction due to economies of scale
        
    def interact(self):
        """Interact with another random homeowner in the neighborhood. This 
        changes the attitude of the current homeowner acording to the opinion 
        dynamics model by Deffuant et al (2002) """
        # not every homeowner interacts 
   
        # Create a list with other homeowners (excl. currently selected agent)
        list_others = [x for x in self.model.schedule.agents if x.unique_id != self.unique_id]
        #print(list_others)
        # Select another random agent
        other = self.random.choice(list_others) 
        #print('Agent {} with opinion {} interacts with Agent {} of opinion {}'
        #      .format(self.unique_id, self.opinion, other.unique_id, other.opinion))
        overlap = min(self.opi+self.unc, other.opi+other.unc) - \
            max(self.opi-self.unc, other.opi-other.unc)
        rel_agr = overlap/self.unc - 1
        if overlap >= self.unc:
            # current agent influences the other one
            opi0 = other.opi
            unc0 = other.unc
            other.opi = round(opi0 + self.model.mu*rel_agr*(self.opi - opi0), 1)
            other.unc = round(unc0 + self.model.mu*rel_agr*(self.unc - unc0), 1)
            print('New opinions & uncertainties for: ', other.unique_id, 
              opi0, other.opi, unc0, other.unc)
        else:
            # curent agent doesn't influence the other
            print('no influence')
        print('Interaction ended for: ', self.unique_id)
        
        
    def interest(self):
        
        """ Evaluate if a homeowner agent is interested in neighborhood-level 
        retrofitting (e.g. reacts to information campaign). These agents will 
        renovate only if benefits from renovation will overweigh the costs. 
        """
        
        if self.adoption_status == False:
            # homeowners have a probability_of_being_interested based on the attitude
            if self.prob_interest>0.5:
                self.interested = True
            else:
                self.interested = False
        else:
            print('already an adopter')
        
        return(self.interested)
        ##helpers
        print('Stage "interest" ended for Homeowner: ', self.unique_id)
        
    def plan(self):
        
        """ Agents who are interested in retrofitting start planning it, i.e. 
        they evaluate various retrofitting options financially. 
        NPV is used to evaluate the financial attractiveness of the 
        retrofitting options that are introduced by the intermediary (e.g. 
        energy consultants). 
        """
        
        if self.interested == True and self.adoption_status==False:
            ### evaluate retrofitting options available using NPV (fin)
            # create the cashflows from the retrofitting options
            
            # building's initial thermal energy demand
            e_0 = self.building['Specific heating demand before [kWh/m2]']*self.area 
            print(e_0)
            num_adoptions = sum([agent.adoption_status for agent in self.model.schedule.agents])
            if num_adoptions > 0:
                self.r_o['Cost [EUR/m2]'] = self.r_o['Cost [EUR/m2]'].values* \
                    (1 - self.group_factor*log(num_adoptions))
            else:
                self.r_o['Cost [EUR/m2]'] = self.r_o['Cost [EUR/m2]'].values
            c_inv_temp = self.r_o['Cost [EUR/m2]']*self.area
            c_inv = c_inv_temp.values[:, np.newaxis]
            e_n_temp = self.r_o['Specific heating demand after [kWh/m2]']*self.area 
            e_n = e_n_temp.values[:, np.newaxis]
            c_n = e_n*self.prices.loc['el'].values
            c_0 = e_0*self.prices.loc['gas'].values
            c_saving = c_0 - c_n 
            e_saving = e_0-e_n 
            print(c_0)
     
            if 'Heating demand reduction [kWh]' not in self.r_o.columns:
                self.r_o['Heating demand reduction [kWh]'] = e_saving
            else: 
                self.r_o.drop('Heating demand reduction [kWh]', axis=1, inplace=True)
    
                self.r_o['Heating demand reduction [kWh]'] = e_saving
            #print(self.r_o)
            cashflow = np.c_[-c_inv, c_saving]
            new_col = []
            for index, i in enumerate(cashflow):
                n = np.npv(rate=self.discount_rate, values=i)
                new_col.append(n)
            if 'NPV' not in self.r_o.columns:
                self.r_o['NPV'] = new_col
                
            else: 
                self.r_o.drop('NPV', axis=1, inplace=True)
                self.r_o['NPV'] = new_col
            print(self.r_o)
            
            
            # agent will not renovate if NPV is negative, i.e. no profitable packages options
            self.profitable = self.r_o[self.r_o['NPV']>=0]
            
            if self.profitable.empty:
                print('No profitable options')
                self.adoption_status = False
            else: # if NPV is positive, then agent uses his weight factors to make a decision
                ro_norm  = self.profitable[['Heating demand reduction [kWh]', 'NPV']].copy()
                norm_values=(ro_norm-ro_norm.min())/(ro_norm.max()-ro_norm.min())
                print(norm_values)
                utility = self.factors[0]*norm_values['NPV'] + \
                    self.factors[1]*norm_values['Heating demand reduction [kWh]']
                if 'Utility' not in self.profitable.columns:
                    self.profitable['Utility'] = utility
                else: 
                    self.profitable.drop('Utility', axis=1, inplace=True)
                    self.profitable['Utility'] = utility
                
                self.preferred = self.profitable[self.profitable['Utility']== \
                                                 self.profitable['Utility'].max()]
                
            #print('Agent', self.unique_id, 'prefers Option \n', self.preferred)
        else: 
            print('Not planning')
        
        print('Stage "planning" ended for Homeowner: ', self.unique_id)
        
    def influence(self):
        """ Influence of an intermediary """
        ## influence of an intermediary 
        
        if self.preferred is None:
            print('No preferred options')
            self.adopted_option = None
            self.adopted_option_name = None
            self.adoption_status = False
        else: # if other profitable options' NPVs lie within the threshold, 
        #the option with the highest energy savings is suggested (i.e. lowest 'Heating demand after')
            diff_npv = self.profitable['NPV'] - self.preferred['NPV']
            suggested = self.profitable.loc[self.profitable[abs(diff_npv) < 1000]['Heating demand reduction [kWh]'].idxmax()]
            print(self.preferred.index[0])
            if suggested.name == self.preferred.index[0]:
                print('No suggested options')
                self.adopted_option = self.preferred
                self.adopted_option_name = self.preferred.index[0]
                self.adoption_status = True
            else: 
                self.adopted_option = suggested
                self.adopted_option_name = suggested.name
                self.adoption_status = True
        
        print('Adopted option of ', self.unique_id, 'is', self.adopted_option_name)
        print('Stage "influence" ended for Homeowner: ', self.unique_id)
        
        # elif self.goal == 'env':
        #     # adopt the option with highest energy savings - what about money?
        #     energy_savings = self.building['Energy initial'] - \
        #         self.r_o['Energy after'] 
        #     result=pd.concat([self.r_o, energy_savings.rename('Energy savings')], axis=1)
        #     self.adopted_package = result.loc[result['Energy savings'].idxmax()]
        #     self.adoption_status = True

        # elif self.goal == 'soc':
        #     # choose whatever others chose
        #     list_others = [x for x in self.model.schedule.agents if isinstance(x, Homeowner) 
        #                and x.unique_id != self.unique_id]
        #     others_choices = []
        #     for agent in list_others:
        #         others_choices.append(agent.adopted_option)
        #     # if nobody has adopted anything
        #     if all(i is None for i in others_choices):
        #         print('No agent in the neighborhood adopted yet')
        #     else:
        #         # clean the list from None goals
        #         chosen_options = [i for i in others_choices if i]
        #         unique = dict(zip(chosen_options,[chosen_options.count(i) for i in chosen_options]))
        #         print("Options adopted by others: ", len(unique))
                
        print('Stage "influence" ended for: ', self.unique_id)
        
        # remove the adopters from model schedule
        #if self.adoption_status == True:
            #self.model.schedule.remove(self)
            
        


        
        

# class Intermediary(Agent):
#     """ An intermediary agent, such as One Stop Shop, that introduces 
#     the neighborhood retrofitting program (informs, presents retrofitting 
#     options, and can influence owner agent decisions)
#     """
    
#     def __init__(self, unique_id, model,target_pop, num_info_rounds=3): 
#         # initialize the parent class with required parameters
#         super().__init__(unique_id, model)
#         ## PARAMETERS
#         self.total_info_rounds = num_info_rounds
#         #self.target_pop = target_pop #target_pop here or in method?
#         self.info_round = 0
#         self.target_pop = target_pop
        
#     def interest(self):
#         # conduct an information campaign
#         self.info_round += 1
#         #inform everyone VS inform certain agents?
#         #if self.target_pop==self.model.schedul.get_agent_count():
#         # for i in range(len(self.model.schedule.agents)):
#         #     self.model.schedule.agents_by_type[Homeowner][i].informed += 1
        
#         print('Info round no.', self.info_round)
#         print('Stage "interest" ended for: ', self.unique_id)
        
#     def plan(self):
#         # introduce the options available for retrofitting, e.g. via a platform
#         print('Stage "plan" ended for: ', self.unique_id)
        
        
#     def decision(self):
#         print('Stage "decision" ended for: ', self.unique_id)
        
        
#     def interact(self):
#         print('Stage "interact" ended for: ', self.unique_id)
        
        
# class Building():
#     def __init__(self, archetype, area, energy_demand, 
#                  energy_type):
#         # Building parameters --> to be used for defining Retrofit options
#         # self.building_id = building_id
#         self.archetype = archetype
#         self.area = area
#         self.energy_demand = energy_demand
#         self.energy_type = energy_type
        

# class Retrofit():
#     def __init__(self, retrofit_id, name, energy_savings, cost, energy_type):
#         self.retrofit_id = retrofit_id
#         self.name = name
#         self.energy_savings = energy_savings
#         self.cost = cost
#         self.energy_type = energy_type
        
        